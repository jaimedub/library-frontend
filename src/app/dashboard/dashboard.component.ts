import { Component, OnInit } from '@angular/core';
import { BookService } from '../book.service';
import { CategoryService } from '../category.service';

declare var jQuery:any;
declare var $:any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  books = [];

  book ={
    name:"",
    author:"",
    published_date: "",
    category_id: ""
  };

  upBook = {};



  category = {
    name:"",
    description:""
  }

  searchStr = "";

  categories = [];

  constructor(
    private bookService:BookService, 
    private categoryService:CategoryService
  ) { }


  ngOnInit() {
    this.getBooks();
    this.getCategories();
  }


  search(){
    this.bookService.search(this.searchStr).subscribe(
      data => {
        this.books = data;
        console.log(data);
      },
      error => {
        console.log(error);
      }
    );
  }


  getBooks(){
    this.bookService.getBooks().subscribe(
      data => {
        this.books = data;
      },
      error => {
        console.log(error);
      }
    );
  }

  getBook(id:any){
    this.bookService.getBook(id).subscribe(
      data => {
        this.upBook = data;
      },
      error => {
        console.log(error);
      }
    );
  }


  postBook(){
    this.bookService.postBook(this.book).subscribe(
      data => {
        console.log(data);
        this.getBooks();
        $('#addModal').modal('hide');
      },
      error => {
        console.log(error);
      }
    );
  }


  updateBook(id:any){
    this.bookService.updateBook(this.upBook, id).subscribe(
      data => {
        console.log(data);
        this.getBooks();
        $('#updateModal').modal('hide');
      },
      error => {
        console.log(error);
      }
    );
  }


  deleteBook(id:any){

    if(confirm("Are you sure you want to delete this book?")){
      this.bookService.deleteBook(id).subscribe(
        data => {
          this.getBooks();
        },
        error => {
          console.log(error);
        }
      );
    }
    
  }


  changeStatus(status:any, id:any){
    this.bookService.changeStatus(status, id).subscribe(
      data => {
        this.getBooks();
      },
      error => {
        console.log(error);
      }
    );
  }


  getCategories(){
    this.categoryService.getCategories().subscribe(
      data => {
        this.categories = data;
      },
      error => {
        console.log(error);
      }
    );
  }

  postCategory(){
    this.categoryService.postCategory(this.category).subscribe(
      data => {
        this.getCategories();
        $('#addCategoryModal').modal('hide');
      },
      error => {
        console.log(error);
      }
    );
  }




}
