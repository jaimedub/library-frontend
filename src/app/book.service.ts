import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class BookService {

  private url = "http://localhost:8000/api";

  constructor(private http:HttpClient) { }


  search(str:any):Observable<any>{
    return this.http.post<any>(`${this.url}/search`, {"search": str}, httpOptions).pipe(catchError(error => throwError(error)));
  }

  getBooks():Observable<any>{
    return this.http.get<any>(`${this.url}/books`).pipe(catchError(error => throwError(error)));
  }

  getBook(id:any):Observable<any>{
    return this.http.get<any>(`${this.url}/book/${id}`).pipe(catchError(error => throwError(error)));
  }

  postBook(book:any): Observable<any> {
    return this.http.post<any>(`${this.url}/book`, book, httpOptions).pipe(catchError(error => throwError(error)));
  }

  updateBook(book:any, id:any): Observable<any> {
    return this.http.put<any>(`${this.url}/book/${id}`, book, httpOptions).pipe(catchError(error => throwError(error)));
  }


  deleteBook(id:any): Observable<any> {
    return this.http.delete<any>(`${this.url}/book/${id}`).pipe(catchError(error => throwError(error)));
  }


  changeStatus(status:any, id:any){
    return this.http.put<any>(`${this.url}/book/${id}/status`, {"available": status}, httpOptions).pipe(catchError(error => throwError(error)));
  }


}
