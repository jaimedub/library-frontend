import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private url = "http://localhost:8000/api";

  constructor(private http:HttpClient) { }

  getCategories():Observable<any>{
    return this.http.get<any>(`${this.url}/categories`).pipe(catchError(error => throwError(error)));
  }

  postCategory(category:any): Observable<any> {
    return this.http.post<any>(`${this.url}/category`, category, httpOptions).pipe(catchError(error => throwError(error)));
  }


}
