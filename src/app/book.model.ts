export class Book{
    id:number;
    name:string;
    author:string;
    published_date:Date;
}